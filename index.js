// console.log("Hello World");

// let nickname = "Tolits"

function printInput(){
	// let nickname = prompt("Enter your nickname:");
	console.log("Hi, "+nickname);
}

// printInput();

// Consider this function
	// Parameter passing
	
				// parameter
	function printName(name, age, address){
		console.log("My name is " +name);
		console.log("My age is " +age)
	}

	//argument
	printName("Angelito", 18);
	printName("Tolits");
	printName();

// [SECTION] Parameters and Arguments

	// Parameter
		// "name" is called a parameter
		// acts as a named variable/container that only exists inside a function.
		// it is used to store information that is provided to a function when it is called/invoked.

	// Arguments
		// "Tolits" is the value/data passed directly into the function.
		// Values passed when invoking  a function are called arguments.
		// Those arguments are then stored as the parameters within the function.


	function checkDivisibilityBy8(num){
		let remainder = num % 8;
		console.log("The remainder of " + num + " divided by 8 is: " + remainder);
		let isDivisibleBy8 = remainder === 0;
		console.log("Is " + num + " divisible by 8?");
		console.log(isDivisibleBy8);
	}

	checkDivisibilityBy8(10);
	checkDivisibilityBy8(32);

	// A variable as the argument.
	let num1 = 64;
	checkDivisibilityBy8(num1);
	checkDivisibilityBy8(num1);

// [SECTION] Function as Arguments
	
	// Function Parameters can also accepts other function as arguments.
	// Some complex functions use other functions as arguments to perform more complicated results.
	// This be further seen in array methods

	function argumentFunc(){
		console.log("This function was passed as an argument before the message was printed.")
	}

	function invokeFunc(argFunc){
		// console.log(argFunc);
		argFunc();
	}
	// Adding and removing the parenthesis "()" impacts the output of javascript.
		//function is used with parenthesis it denotes invoke/call.
		//function is used without a parenthesis is normally associated with using a function as an argument to another function.

	invokeFunc(argumentFunc);

// [SECTION] Mutiple Paramters
	
	// Mutiple "arguments" will correspond to the number of "parameters" declared in a function in succeeding order.

	function createFullName(firstName, middleName, lastName){
		console.log(firstName+" "+middleName+" "+lastName);
	}

	createFullName("Juan", "Enye", "Dela Cruz");
	createFullName("Juan", "Dela Cruz");
	createFullName("Juan", "Enye", "Dela Cruz", "Junior");

	// Using variables as arguments
	let firstName = "John";
	let middleName = "Doe";
	let lastName = "Smith";

	createFullName(firstName, middleName, lastName);

	// Note: The order of the "arguments" is the same to the order of the "parameters". The first argument will be stored in the first parameter, second argument wil be stored in the second parameter and so on.

// [SECTION] Return Statement
	// allows us to output a value from a function to be passed to the line/code block of the code that invoked/called the function.
	// we can further use/manipulate in our program insted of only printing/displaying it on the console.

	function returnFullName(firstName, middleName, lastName){
		//expected return Jeffrey Smith Doe
		return firstName +" "+ middleName+" "+lastName;

		// return indicates the end of function execution
		// it will ignore any codes after return statement.
		console.log(firstName +" "+ middleName+" "+lastName);
	}

	let completeName = returnFullName("Jeffrey", "Smith", "Doe");
	console.log(completeName);

	console.log("My complete name is " +completeName);
	console.log("I am "+ completeName +" I live in Quezon City.");

	function checkDivisibilityBy5(num){
		let rem = remainder(num);
		console.log("The remainder of " + num + " divided by 5 is: " + rem);
		let isDivisibleBy5 = rem === 0;
		console.log("Is " + num + " divisible by 5?");
		console.log(isDivisibleBy5);
	}

	function remainder(num){
		return num % 5;
	}

	checkDivisibilityBy5(21);

	// You can also create a variable inside the function to contain the result and return the variable.

	function returnAddress(city, country){
		let fullAddress = city + ", " + country;
		return fullAddress;
	}

	let myAddress = returnAddress("Cebu City", "Cebu");
	console.log(myAddress);
	console.log(returnAddress("Cebu City", "Cebu"));

	// when a function only has a console.log() to display the result it will return undefined instead.
	function printPlayerInfo(username, level, job){
		console.log("Username: "+username);
		console.log("Level: " +level);
		console.log("Job: " +job);
	}

	let user = printPlayerInfo("knight_white", 95, "Paladin");
	// You cannot save any value from printPlayerInfo() because it does not return anything.
	console.log(user);
